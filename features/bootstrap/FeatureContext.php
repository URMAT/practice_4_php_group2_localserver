<?php

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends AttractorContext
{
    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     */
    public function яВижуСловоГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я вижу 3 слово "([^"]*)" "([^"]*)" "([^"]*)" где\-то на странице$/
     */
    public function яВижу3СловоГдеТоНаСтранице($arg1, $arg2, $arg3)
    {
        $this->assertPageContainsText($arg1, $arg2, $arg3);
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('main_page'));
    }

    /**
     * @When /^кликаю на ссылку зарегестрируйтесь/
     */
    public function КликаюНаСсылкуЗарегестрируйтесь()
    {
        $this->clickLink('зарегистрируйтесь');
    }
    /**
     * @When /^кликаю на ссылку авторизуйтесь/
     */
    public function КликаюНаСсылкуАвторизуйтесь()
    {
        $this->clickLink('авторизуйтесь');
    }

    /**
     * @When /^кликаю на ссылку выход/
     */
    public function КликаюНаСсылкуВыход()
    {
        $this->clickLink('Выход');
    }
    /**
     * @When /^кликаю На ссылку главную/
     */
    public function КликаюНаСсылкуГлавную()
    {
        $this->clickLink('На главную');
    }


    /**
     * @When /^кликаю на ссылку Зарегистрировать новый объект/
     */
    public function кликаюНаСсылкуЗарегистрироватьНовыйОбъект()
    {
        $this->clickLink('Зарегистрировать новый объект');
    }


    /**
     * @When /^я заполняю поля регистрации/
     */
    public function яЗаполняюПоляРегистрациии(){
        $this->fillField('register_email', "MegaTEST2@gmail.com");
        $this->fillField('register_passport', "p2w1sd2wqwqw");
        $this->fillField('register_password', "123");
        $this->fillField('register_roles', 'landlord');
    }
    /**
     * @When /^я заполняю поля авторизации/
     */
    public function яЗаполняюПоляАвторизации(){
        $this->fillField('auth_email', "mytest@gmail.com");
        $this->fillField('auth_password', "123");
    }

    /**
     * @When /^я заполняю поля формы регистрации нового объекта/
     */
    public function яЗаполняюПоляФормыегистрацииНовогоОбъекта(){
        $this->fillField('registration_object_kind_object', 'pension');
        $this->fillField('registration_object_name_object', "Test");
        $this->fillField('registration_object_full_name', "Test TEst Testovich");
        $this->fillField('registration_object_quantity_room', "12");
        $this->fillField('registration_object_price', 100000);
        $this->fillField('registration_object_contact_phone', '0700-31-22-13');
//        $this->fillField('registration_object_address', '41.919623920941234,77.40278092903449');
        $this->setMinkParameter('registration_object_address', '41.919623920941234,77.40278092903449');
    }

    /**
     * @When /^я заполняю поле Поиск по фразе/
     */
    public function яЗаполняюПолеПоискПоФразе(){
        $this->fillField('search_filter_word', 'ль');
    }

    /**
     * @When /^я делаю фильтрацию по цене/
     */
    public function яДелаюФильтрациюПоЦене(){
        $this->fillField('search_filter_min_price', 1000);
        $this->fillField('search_filter_max_price', 1500);
    }


    /**
     * @When /^я указываю опции объекта/
     */
    public function яУказываюОпцииОбъекта(){
        $this->fillField('pension_cook', true);
        $this->fillField('pension_tv', true);
        $this->fillField('pension_swimming_pool', true);
    }

    /**
     * @When /^жму кнопку Save/
     */
    public function жмуКнопкуSave()
    {
        $this->pressButton('Save');
    }

    /**
     * @When /^жму кнопку Search/
     */
    public function жмуКнопкуSearch()
    {
        $this->pressButton('Search');
        sleep(3);
    }

    /**
     * @When /^жму кнопку Next/
     */
    public function жмуКнопкуNext()
    {
        $this->pressButton('Next');
    }

    /**
     * @When /^жму кнопку Login/
     */
    public function жмуКнопкуLogin()
    {
        $this->pressButton('Login');
    }
}