<?php

/**
 * Defines application features from the specific context.
 */
class LoginContext extends AttractorContext
{
    /**
     * @When /^кликаю на ссылку зарегестрируйтесь/
     */
    public function КликаюНаСсылкуЗарегестрируйтесь()
    {
        $this->clickLink('зарегистрируйтесь');
    }

    /**
     * @When /^я заполняю поля регистрации/
     */
    public function яЗаполняюПоляРегистрациии(){
        $this->fillField('register_email', "tw231s9t@teswt.com");
        $this->fillField('register_passport', "12As2i0N3Т4eь");
        $this->fillField('register_password', "123");
        $this->fillField('register_roles', 'landlord');
    }
    /**
     * @When /^жму кнопку Save/
     */
    public function жмуКнопкуSave()
    {
        $this->pressButton('Save');
    }
}