<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\SearchFilterType;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\BookingObject\BookingObjectHandler;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{
    /**
     * @Route("/sign-up", name="app-sign-up")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function signUpAction(
        ApiContext $apiContext,
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $error = null;
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                if ($apiContext->clientExists($user->getPassport(), $user->getEmail())) {
                    $error = 'Error: Вы уже зарегистрированы. Вам нужно авторизоваться.';
                } else {
                    $data = [
                        'email' => $user->getEmail(),
                        'passport' => $user->getPassport(),
                        'password' => $user->getPassword(),
                        'roles' => $user->getRoles()
                    ];

                    $apiContext->createClient($data);

                    if(in_array('ROLE_LANDLORD' ,$data['roles']))
                    {
                        $user = $userHandler->createNewLandlord($data);
                    }
                    elseif (in_array('ROLE_TENANT' ,$data['roles']))
                    {
                        $user = $userHandler->createNewTenant($data);
                    }
                    else{
                        $user = $userHandler->createNewUser($data['roles']);
                    }

                    $manager->persist($user);
                    $manager->flush();

                    $userHandler->makeUserSession($user);

                    return $this->redirectToRoute('app_profile_page');

                }
            } catch (ApiException $e) {
                $error = 'Error: ' . $e->getMessage() . '  |||  ' . var_export($e->getResponse(), 1);
            }

        }

        return $this->render('sign_up.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }


    /**
     * @Route("/", name="main_page")
     * @throws ApiException
     */
    public function indexAction(
        ApiContext $apiContext,
        BookingObjectHandler $bookingObjectHandler,
        Request $request
    )
    {
        $currentUser = $this->getUser();
        $isLandlord = false;
        if($currentUser){
            if(in_array('ROLE_LANDLORD', $currentUser->getRoles())){
                $isLandlord = true;
            }
        }
        $arrayObjects = [];
        $allBookingObject = $apiContext->takeAllBookingObject();
        dump($allBookingObject);

        for($i = 0; $i < count($allBookingObject); $i++){
            if($allBookingObject[$i]['entity_name'] == "App\Entity\Pension"){
                $arrayObjects [] = $bookingObjectHandler->createNewPension($allBookingObject[$i]);
            }
            if($allBookingObject[$i]['entity_name'] == "App\Entity\Cottage"){
                $arrayObjects [] = $bookingObjectHandler->createNewCottage($allBookingObject[$i]);
            }
        }

        $formSearchFilter = $this->createForm('App\Form\SearchFilterType');
        $formSearchFilter->handleRequest($request);
        if($formSearchFilter->isSubmitted() && $formSearchFilter->isValid()){
            $data = $formSearchFilter->getData();
            $result = $apiContext->sortObject($data);
            $arrayObjects = [];
            for($i = 0; $i < count($result); $i++){
                if($result[$i]['entity_name'] == "App\Entity\Pension"){
                    $arrayObjects [] = $bookingObjectHandler->createNewPension($result[$i]);
                }
                if($result[$i]['entity_name'] == "App\Entity\Cottage"){
                    $arrayObjects [] = $bookingObjectHandler->createNewCottage($result[$i]);
                }
            }
        }
        return $this->render('main_page.html.twig', [
                    'isUser' => $currentUser,
                    'objects' => $arrayObjects,
                    'formSearchByWord' => $formSearchFilter->createView(),
                    'isLandlord' => $isLandlord,
                ]
            );
    }

    /**
     * @Route("/booking_object_details/{id}", name="booking_object_details")
     * @param $id
     * @param ApiContext $apiContext
     * @param BookingObjectHandler $bookingObjectHandler
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     * @throws ApiException
     */
    public function bookingObjectDetailsAction($id, ApiContext $apiContext, BookingObjectHandler $bookingObjectHandler) {
            $objectData = $apiContext->getObjectById($id);
            $arrayObjects = null;
            for($i = 0; $i < count($objectData); $i++){
                if($objectData[$i]['entity_name'] == "App\Entity\Pension"){
                    $arrayObjects = $bookingObjectHandler->createNewPension($objectData[$i]);
                }
                if($objectData[$i]['entity_name'] == "App\Entity\Cottage"){
                    $arrayObjects = $bookingObjectHandler->createNewCottage($objectData[$i]);
                }
            }
            dump($objectData);
            $kind_object = get_class($arrayObjects);
            $isPension = false;
            $isCottage = false;
                if($kind_object == "App\Entity\Pension"){
                    $isPension = true;
                }
                if($kind_object == "App\Entity\Cottage"){
                    $isCottage = true;
                }

            return $this->render('object_details.html.twig', [
                'object' => $arrayObjects,
                'isPension' => $isPension,
                'isCottage' => $isCottage
            ]);
    }


    /**
     * @Route("/check-connect", name="ping")
     */
    public function checkConnectionWithSeverAction(ApiContext $apiContext)
    {
        try {
            return new Response(var_export($apiContext->makePing(), true));
        } catch (ApiException $e) {
            return new Response('Error: ' . $e->getMessage());
        }
    }

    /**
     * @Route("/auth", name="auth")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @param ApiContext $apiContext
     * @param ObjectManager $manager
     * @return Response
     */
    public function authAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler,
        ApiContext $apiContext,
        ObjectManager $manager
    )
    {
        $error = null;

        $form = $this->createForm("App\Form\AuthType");

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $user = $userRepository->getByCredentials(
                $data['password'],
                $data['email']
            );

            if ($user) {
                $userHandler->makeUserSession($user);

                return $this->redirectToRoute('app_profile_page');
            }

            try {
                if ($apiContext->checkClientCredentials(
                    $data['password'],
                    $data['email']
                )) {
                    $centralData = $apiContext->getClientByEmail($data['email']);

                    $user = $userHandler->createNewUser(
                        $centralData,
                        false
                    );
                    $manager->persist($user);
                    $manager->flush();
                    $userHandler->makeUserSession($user);
                    return $this->redirectToRoute('main_page');
                } else {
                    $error = 'Ты не тот, за кого себя выдаешь';
                }
            } catch (ApiException $e) {
                $error = 'Что-то где-то пошло нетак';
            }
        }

        return $this->render(
            '/sign_in.html.twig', [
            'form' => $form->createView(),
            'error' => $error
        ]);
    }

    /**
     * @Route("/add_object", name="addObject")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     */
    public function registrationObjectForBookingAction(
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $form = $this->createForm("App\Form\RegistrationObjectType");

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataFromForm = $form->getData();
            $this->get('session')->set('basic_data', $dataFromForm);
            if($dataFromForm['kind_object'] == 'pension'){
                return $this->redirectToRoute('pension');
            }elseif ($dataFromForm['kind_object'] == 'cottage'){
                return $this->redirectToRoute('cottage');
            }

        }
        return $this->render('form_for_registration_object.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/pension", name="pension")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @throws ApiException
     */
    public function pensionAction(
        Request $request,
        ApiContext $apiContext
    )
    {
        $form = $this->createForm("App\Form\PensionType");

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataFromForm = $form->getData();
            $dataFromFirstForm = $this->get('session')->get('basic_data');

            $data = array_merge($dataFromFirstForm, $dataFromForm);

            $apiContext->registerPension($data);
            return $this->redirectToRoute('main_page');
        }
        return $this->render('pension_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/cottage", name="cottage")
     * @param ApiContext $apiContext
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @throws ApiException
     */
    public function cottageAction(
        Request $request,
        ApiContext $apiContext
    )
    {
        $form = $this->createForm("App\Form\CottageType");

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dataFromForm = $form->getData();
            $dataFromFirstForm = $this->get('session')->get('basic_data');

            $data = array_merge($dataFromFirstForm, $dataFromForm);

            $apiContext->registerCottage($data);

            return $this->redirectToRoute('main_page');
        }
        return $this->render('cottage_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/qwe", name="qwe")
     * @param ApiContext $apiContext
     * @return Response
     * @throws ApiException
     */
    public function qweAction(ApiContext $apiContext)
    {
        $result = $apiContext->clientExists('some & passport', '123@123.ru');
        return new Response(var_export($result, true));
    }
}
