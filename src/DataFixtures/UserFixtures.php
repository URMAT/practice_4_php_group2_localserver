<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/13/18
 * Time: 7:39 PM
 */

namespace App\DataFixtures;

use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    public function load(ObjectManager $manager)
    {

        $user = $this->userHandler->createNewUser([
            'email' => 'mytest@gmail.com',
            'passport' => 'some & passport',
            'password' => '123',
            'roles' => ["ROLE_USER","ROLE_LANDLORD"],
        ]);

        $manager->persist($user);
        $manager->flush();
    }
}
