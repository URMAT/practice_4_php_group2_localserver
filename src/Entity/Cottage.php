<?php

namespace App\Entity;

class Cottage extends BookingObject
{
    private $kitchen;

    private $spa;

    private $internet;

    public function __toArray(){
        $cottage = [];
        $cottage['kitchen'] = $this->getKitchen();
        $cottage['address'] = $this->getAddress();
        $cottage['full_name'] = $this->getFullName();
        $cottage['contact_phone'] = $this->getContactPhone();
        $cottage['price'] = $this->getPrice();
        $cottage['name_object'] = $this->getNameObject();
        $cottage['internet'] = $this->getInternet();
        $cottage['spa'] = $this->getSpa();
        $cottage['quantity_room'] = $this->getQuantityRoom();
        return $cottage;
    }

    public function setKitchen($kitchen)
    {
        $this->kitchen = $kitchen;
        return $this;
    }

    public function getKitchen()
    {
        return $this->kitchen;
    }

    public function setSpa($spa)
    {
        $this->spa = $spa;
        return $this;
    }

    public function getSpa()
    {
        return $this->spa;
    }

    public function setInternet($internet)
    {
        $this->internet = $internet;
        return $this;
    }

    public function getInternet()
    {
        return $this->internet;
    }


}
