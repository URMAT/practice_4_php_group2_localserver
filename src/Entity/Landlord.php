<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LandlordRepository")
 */
class Landlord extends User
{



    /**
     * @var string
     * @ORM\Column(type="string", length=1024, nullable=true)
     */
    private $full_name;

    /**
     * @param string $full_name
     * @return Landlord
     */
    public function setFullName(string $full_name): Landlord
    {
        $this->full_name = $full_name;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return $this->full_name;
    }


}
