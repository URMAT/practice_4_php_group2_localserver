<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CottageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kitchen', CheckboxType::class, [
                    'label'    => 'Нужен ли вам коттедж с отдельной кухней?',
                    'required' => false,
                ]
            )
            ->add('internet', CheckboxType::class, [
                    'label'    => 'Нужен ли вам интернет?',
                    'required' => false,
                ]
            )
            ->add('spa', CheckboxType::class, [
                    'label'    => 'Хотите ли вы посещать SPA?',
                    'required' => false,
                ]
            )
            ->add('save', SubmitType::class)
        ;
    }
}
