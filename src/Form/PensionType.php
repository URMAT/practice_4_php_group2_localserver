<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class PensionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cook', CheckboxType::class, [
                    'label'    => '3х - разовое питание?',
                    'required' => false,
                ]
            )
            ->add('tv', CheckboxType::class, [
                    'label'    => 'Нужно ли вам кабельное телевидение?',
                    'required' => false,
                ]
            )
            ->add('swimming_pool', CheckboxType::class, [
                    'label'    => 'Хотите ли вы посещать плавательный бассейн?',
                    'required' => false,
                ]
            )
            ->add('save', SubmitType::class)
        ;
    }
}
