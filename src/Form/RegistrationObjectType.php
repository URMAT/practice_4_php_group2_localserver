<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class RegistrationObjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kind_object', ChoiceType::class, [
                'label' => 'Выберите тип объекта',
                'placeholder' =>  'Выберите тип',
                'choices' => [
                    'Пансионат' => 'pension',
                    'Коттедж' => 'cottage'
                ],
            ])
            ->add('name_object', TextType::class, [
                'label' => 'Укажите название объекта'
            ])
            ->add('full_name', TextType::class, [
                'label' => 'Укажите контактное лицо'
            ])
            ->add('quantity_room', TextType::class, [
                'label' => 'Укажите укажите количество номер'
            ])
            ->add('price', NumberType::class, [
                'label' => 'Укажите цену за сутки'
            ])
            ->add('contact_phone', TelType::class, [
                'label' => 'Введите номер телефона по форме 0700-11-22-33',
                'attr' => [
                    'pattern' => '0[0-9]{3}-[0-9]{2}-[0-9]{2}-[0-9]{2}'
                ]
            ])
            ->add('address', TextType::class, [
                'label' => 'Кликните на карту чтобы указать адрес',
                'attr' => [
                    'readonly' => true
                ]
            ])
            ->add('Next', SubmitType::class)
        ;
    }
}
