<?php
namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class SearchFilterType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'word',
                TextType::class, [
                    'label' => 'Поиск по фразе',
                    'required' => false
                ]
            )
            ->add('min_price', IntegerType::class, [
                'required' => false,
                'label' => 'Минимальная цена от'
            ])
            ->add('max_price', IntegerType::class,[
                'required' => false,
                'label' => 'Максимальная цена до'
            ])
            ->add('kind_object', ChoiceType::class, [
                'label' => 'Выберите тип объекта бронирования:',
                'placeholder' => 'Выберите',
                'required' => false,
                'choices' => [
                    'Пансионат' => 'pension',
                    'Коттедж' => 'Cottage'
                ],
            ])
            ->add('Search', SubmitType::class)
        ;
    }
}
