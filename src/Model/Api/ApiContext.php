<?php

namespace App\Model\Api;


use Curl\Curl;
use Symfony\Bundle\FrameworkBundle\Tests\CacheWarmer\testRouterInterfaceWithoutWarmebleInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_PING = '/ping';
    const ENDPOINT_CLIENT = '/client';
    const ENDPOINT_REGISTRATION_PENSION = '/registration_pension';
    const ENDPOINT_REGISTRATION_COTTAGE = '/registration_cottage';
    const ENDPOINT_All_BOOKING_OBJECT = '/take_booking_object';
    const ENDPOINT_GET_BY_ID = '/get_by_id';
    const ENDPOINT_CONCRETE_CLIENT_BY_EMAIL = '/client/email/{email}';
    const ENDPOINT_FIND_BY_WORD = '/find_by_word';
    const ENDPOINT_SORT_MIN_OR_MAX = '/sort_min_or_max';
    const ENDPOINT_CONCRETE_CLIENT = '/client/{passport}/{email}';
    const ENDPOINT_CHECK_CLIENT_CREDENTIALS = '/check_client_credentials/{encodedPassword}/{email}';

    /**
     * @return mixed
     * @throws ApiException
     */
    public function makePing()
    {
        return $this->makeQuery(self::ENDPOINT_PING, self::METHOD_GET);
    }

    /**
     * @return mixed
     * @throws ApiException
     */
    public function takeAllBookingObject()
    {
        return $this->makeQuery(self::ENDPOINT_All_BOOKING_OBJECT, self::METHOD_GET);
    }

    /**
     * @return mixed
     * @throws ApiException
     */
    public function getObjectById($id)
    {
        $data ['id'] = $id;
        return $this->makeQuery(self::ENDPOINT_GET_BY_ID, self::METHOD_GET, $data);
    }

    /**
     * @throws ApiException
     */
    public function sortObject(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_FIND_BY_WORD, self::METHOD_GET, $data);
    }
    /**
     * @throws ApiException
     */
    public function sortByMinOrMax(array $value)
    {
        return $this->makeQuery(self::ENDPOINT_SORT_MIN_OR_MAX, self::METHOD_GET, $value);
    }

    /**
     * @param string $passport
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function clientExists(string $passport, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT, [
                'passport' => $passport,
                'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function registerPension($data)
    {
        return $this->makeQuery(self::ENDPOINT_REGISTRATION_PENSION, self::METHOD_POST, $data);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function registerCottage($data)
    {
        return $this->makeQuery(self::ENDPOINT_REGISTRATION_COTTAGE, self::METHOD_POST, $data);
    }

    /**
     * @param string $email
     * @return array
     * @throws ApiException
     */
    public function getClientByEmail(string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CONCRETE_CLIENT_BY_EMAIL, [
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_GET);
    }

    /**
     * @param string $plainPassword
     * @param string $email
     * @return bool
     * @throws ApiException
     */
    public function checkClientCredentials(string $plainPassword, string $email)
    {
        $endPoint = $this->generateApiUrl(
            self::ENDPOINT_CHECK_CLIENT_CREDENTIALS, [
            'encodedPassword' => $this->userHandler->encodePlainPassword($plainPassword),
            'email' => $email
        ]);

        return $this->makeQuery($endPoint, self::METHOD_HEAD);
    }

    /**
     * @param array $data
     * @return mixed
     * @throws ApiException
     */
    public function createClient(array $data)
    {
        return $this->makeQuery(self::ENDPOINT_CLIENT, self::METHOD_POST, $data);
    }
}
