<?php
/**
 * Created by PhpStorm.
 * User: felix
 * Date: 6/16/18
 * Time: 9:53 PM
 */

namespace App\Model\BookingObject;

use App\Entity\Cottage;
use App\Entity\Landlord;
use App\Entity\Pension;
use App\Entity\Tenant;
use App\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class BookingObjectHandler
{

    /**
     * @param array $data
     */
    public function createNewAbstractBookingObject($bookingObject, array $data) {
        $bookingObject
                    ->setId($data['id'])
                    ->setFullName($data['full_name'])
                    ->setNameObject($data['name_object'])
                    ->setAddress($data['address'])
                    ->setContactPhone($data['contact_phone'])
                    ->setPrice($data['price'])
                    ->setQuantityRoom($data['quantity_room']);

        return $bookingObject;
    }

    /**
     * @param array $data
     * @return Pension
     */
    public function createNewPension(array $data) {
        $pension = new Pension();
        $pension->setSwimmingPool($data['swimming_pool'])
                ->setTv($data['tv'])
                ->setCook($data['cook']);
        $pension = $this->createNewAbstractBookingObject($pension, $data);

        return $pension;
    }

    /**
     * @param array $data
     * @param bool $encodePassword
     * @return Cottage
     */
    public function createNewCottage(array $data) {
        $cottage = new Cottage();
        $cottage->setInternet($data['internet'])
                ->setSpa($data['spa'])
                ->setKitchen($data['kitchen']);
        $cottage = $this->createNewAbstractBookingObject($cottage, $data);

        return $cottage;
    }
}
